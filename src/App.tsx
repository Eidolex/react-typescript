import React, { Component, FC, lazy, Suspense } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { RouteProps } from 'react-router';
import { connect, ConnectedProps } from 'react-redux';

import { IStoreState } from './store/types';

import AsyncComponent from './components/AsyncComponent';
import LoadingScreen from './components/LoadingScreen';
import Home from './views/Home';
import Login from './views/Login';
import { setBootStrapAction } from './store/actions/app.action';
import { TokenResponse } from './store/types/auth.type';
import { setAuthToken } from './assets/scripts/api';
import { fetchProfileAction_Async } from './store/actions/auth.action';

const AuthRoute: FC<{ isLogin: boolean } & RouteProps> =
    (props) => {
        const {
            children,
            isLogin,
            ...rest
        } = props;
        return (
            <Route {...rest} render={() => {
                return isLogin ? children : <Redirect push={false} to={{ pathname: "/login" }}/>
            }}/>
        );
    };


const fakePromise = () => new Promise(resolve => {
    setTimeout(() => resolve(), 3000);
});

const LazyComponent = lazy(() => import('./views/Lazy'));

type IProps = {} & ReduxProps;

class App extends Component<IProps> {

    constructor(props: any) {
        super(props);
        this.bootstrap();
    }

    render() {
        const { bootstrap, profile } = this.props;
        return (
            <div className="App">
                {(bootstrap) ? (
                    <>
                        <div className="RouterView">
                            <Router>
                                <Suspense fallback={<LoadingScreen/>}>
                                    <Switch>
                                        <Route exact path="/lazy">
                                            <AsyncComponent promise={fakePromise} children={<LazyComponent/>}/>
                                        </Route>
                                        <Route exact path="/login">
                                            <Login/>
                                        </Route>
                                        <AuthRoute exact path="/" isLogin={!!profile}>
                                            <Home/>
                                        </AuthRoute>
                                    </Switch>
                                </Suspense>
                            </Router>
                        </div>
                    </>
                ) : <LoadingScreen/>}
            </div>
        );
    }

    private bootstrap() {
        const tokenStr = localStorage.getItem('_token') || '';
        if (tokenStr) {
            const tokenResponse: TokenResponse = JSON.parse(tokenStr);
            setAuthToken(tokenResponse.token);
            this.props.fetchProfileAction_Async()
                .then(() => {
                    this.props.setBootStrapAction(true)
                });
        } else {
            this.props.setBootStrapAction(true);
        }
    }
}

const mapStateToProps = ({ appState, authState }: IStoreState) => ({
    bootstrap: appState.bootstrap,
    profile: authState.profile,
});

const mapDispatchToProps = {
    setBootStrapAction,
    fetchProfileAction_Async
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type ReduxProps = ConnectedProps<typeof connector>;

export default connector(App);
