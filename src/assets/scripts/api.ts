let authToken: string | null;

export function setAuthToken(token: string | null) {
    authToken = token;
}

export default function api(input: RequestInfo, init?: RequestInit) {
    init = init || {};

    const defaultHeader: { [key: string]: string } = {
        Accept: 'application/json',
    };

    if (init.method === 'POST' && typeof init.body === 'string') {
        defaultHeader['Content-Type'] = 'application/json';
        defaultHeader['Content-Length'] = init.body.length.toString();
    }

    if (authToken) {
        defaultHeader['Authorization'] = authToken;
    }

    init = {
        ...init,
        headers: {
            ...defaultHeader,
            ...init.headers,
        }
    };

    return fetch(input, init);
}
