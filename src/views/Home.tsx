import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import Page from '../components/Page';
import { IStoreState } from '../store/types';

type IProps = {} & ReduxProps

class Home extends Component<IProps> {
    render() {
        return (
            <Page>
            </Page>
        );
    }
}

const mapStateToProps = (state: IStoreState) => ({
    profile: state.authState.profile
});

const mapDispatchToProps = {};

const connector = connect(
    mapStateToProps,
    mapDispatchToProps
);

type ReduxProps = ConnectedProps<typeof connector>

export default connector(Home);
