import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { IStoreState } from '../store/types';
import Page from '../components/Page';

type IProps = {} & ReduxProps

class Login extends Component<IProps> {
    render() {
        return (
            <Page headerTitle="Login">
                <Link to={{ pathname: '/lazy' }}>Lazy</Link>
            </Page>
        );
    }
}

const mapStateToProps = (state: IStoreState) => ({});

const mapDispatchToProps = {};

const connector = connect(mapStateToProps, mapDispatchToProps);

type ReduxProps = ConnectedProps<typeof connector>;

export default withRouter(connector(Login));
