import React, { PureComponent } from 'react';
import LoadingScreen from './LoadingScreen';

type IProps = {
    promise: () => Promise<any>
    reject?: JSX.Element
}

type IState = {
    resolvedError: boolean,
    resolvedSuccess: boolean,
}

class AsyncComponent extends PureComponent<IProps, IState> {
    /** To prevent memory leak */
    private cancelled: boolean = false;

    constructor(props: IProps) {
        super(props);

        this.state = {
            resolvedSuccess: false,
            resolvedError: false,
        }
    }

    componentDidMount(): void {
        this.props.promise()
            .then(() => {
                if (this.cancelled) return;
                this.setState({
                    resolvedSuccess: true,
                })
            })
            .catch(() => {
                if (this.cancelled) return;
                this.setState({
                    resolvedError: true,
                })
            });
    }

    componentWillUnmount(): void {
        this.cancelled = true;
    }


    render() {
        const { reject, children } = this.props;
        if (this.state.resolvedSuccess) {
            return (
                <>
                    {children}
                </>
            );
        } else if (this.state.resolvedError) {
            return reject ? reject : (<div>Some Error Occurs</div>)
        } else {
            return <LoadingScreen/>
        }
    }
}

export default AsyncComponent;
