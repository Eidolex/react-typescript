import React, { PureComponent } from 'react';
import './Page.css';

type IProp = {
    header?: JSX.Element
    headerTitle?: JSX.Element | string
    headerLeft?: JSX.Element
    headerRight?: JSX.Element
    footer?: JSX.Element,
};

class Page extends PureComponent<IProp> {

    render() {
        return (
            <div className="Page">
                {this.renderHeader()}
                <div className="PageContent">
                    {this.props.children}
                </div>
                {this.props.footer}
            </div>
        );
    }

    private renderHeader(): JSX.Element {
        const {header, headerTitle} = this.props;
        return header
            ? header
            : (
                <div className="PageHeader">
                    <div className="PageHeaderLeft"/>
                    <div className="PageHeaderTitle">
                        {headerTitle}
                    </div>
                    <div className="PageHeaderRight"/>
                </div>
            );
    }
}

export default Page;
