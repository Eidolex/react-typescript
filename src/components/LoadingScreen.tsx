import React, { PureComponent } from 'react';

import './LoadingScreen.css';

type IProps = {
    hidden?: boolean
}

class LoadingScreen extends PureComponent<IProps> {
    render() {
        const {hidden} = this.props;
        return (
            <div className="LoadingScreen" hidden={hidden}>
                Loading
            </div>
        );
    }
}

export default LoadingScreen;
