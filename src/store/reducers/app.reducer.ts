import { IAppAction, IAppState, SET_BOOTSTRAP_ACTION } from '../types/app.type';

const initialState: IAppState = {
    bootstrap: false,
};

export default function reduceAppState(state: IAppState = initialState, action: IAppAction): IAppState {
    const { type } = action;
    if (type === SET_BOOTSTRAP_ACTION) {
        return {
            ...state,
            bootstrap: action.payload
        };
    } else {
        return state;
    }
}
