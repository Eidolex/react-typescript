import { combineReducers, Reducer } from 'redux';
import { IStoreAction, IStoreState } from '../types';

import reduceAuthState from './auth.reducer';
import reduceAppState from './app.reducer';

const reducers: Reducer<IStoreState, IStoreAction> = combineReducers({
    authState: reduceAuthState,
    appState: reduceAppState
});

export default reducers;
