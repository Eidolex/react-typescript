import { IAuthAction, IAuthState, SET_PROFILE_ACTION } from '../types/auth.type';

const initialState: IAuthState = {};

export default function reduceAuthState(state: IAuthState = initialState, action: IAuthAction): IAuthState {
    if (action.type === SET_PROFILE_ACTION) {
        return {
            ...state,
            profile: action.payload
        };
    } else {
        return state;
    }
}
