import { Profile, SET_PROFILE_ACTION, SetProfileAction, TokenResponse } from '../types/auth.type';
import { IThunkAction } from '../types';
import api, { setAuthToken } from '../../assets/scripts/api';
import { FETCH_PROFILE_API_URL, LOGIN_API_URL } from '../../constants/api-routes';

export const setProfileAction = (profile: Profile | null): IThunkAction<void, SetProfileAction> =>
    (dispatch) => {
        dispatch({
            type: SET_PROFILE_ACTION,
            payload: profile
        });
    };


export const fetchProfileAction_Async = (): IThunkAction<Promise<void>, SetProfileAction> =>
    async (dispatch) => {
        const response = await api(FETCH_PROFILE_API_URL);

        if (response.ok) {
            const profile: Profile = await response.json();
            dispatch(setProfileAction(profile));
        }
    };

export const login_Async = (): IThunkAction<Promise<void>, SetProfileAction> =>
    async (dispatch) => {
        const response = await api(LOGIN_API_URL);

        if (response.ok) {
            const tokenResponse: TokenResponse = await response.json();

            setAuthToken(tokenResponse.token);

            await dispatch(fetchProfileAction_Async());
        }
    };
