import { IThunkAction } from '../types';
import {
    SET_BOOTSTRAP_ACTION,
    SetBootStrapAction,
} from '../types/app.type';

export const setBootStrapAction = (val: boolean): IThunkAction<void, SetBootStrapAction> =>
    (dispatch) => {
        dispatch({
            type: SET_BOOTSTRAP_ACTION,
            payload: val
        });
    };
