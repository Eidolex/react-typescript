import { ThunkAction } from 'redux-thunk';
import { Action } from 'redux';

import { IAuthAction, IAuthState } from './auth.type';
import { IAppAction, IAppState } from './app.type';

export type IStoreState = {
    authState: IAuthState
    appState: IAppState
};

export type IStoreAction = IAuthAction | IAppAction;

export type IThunkAction<R, A extends Action> = ThunkAction<R, IStoreState, undefined, A>


