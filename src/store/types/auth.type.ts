export const SET_PROFILE_ACTION = 'SET_PROFILE_ACTION';

export type Profile = {
    id: number
    name: string
}

export type TokenResponse = {
    token: string
}

export type IAuthState = {
    profile?: Profile | null
}

export type SetProfileAction = {
    type: typeof SET_PROFILE_ACTION
    payload: Profile | null
}

export type IAuthAction = SetProfileAction;
