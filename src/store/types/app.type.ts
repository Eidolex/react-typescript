export const SET_BOOTSTRAP_ACTION = 'SET_BOOT_STRAP_ACTION';

export type IAppState = {
    bootstrap: boolean,
};

export type SetBootStrapAction = {
    type: typeof SET_BOOTSTRAP_ACTION,
    payload: boolean,
}

export type IAppAction = SetBootStrapAction;
